import pygame

from src.logic.color import Color
from src.logic.goban import Goban
from src.logic.stone import Stone

from src.ui.ui import UserInterface


class MainUserInterface(UserInterface):

    WINDOW_SIZE = (389, 389)

    """ Assets. """

    BACKGROUND_IMAGE = "assets/background.png"

    WHITE_PREVIEW_IMAGE = "assets/white_preview.png"

    BLACK_PREVIEW_IMAGE = "assets/black_preview.png"

    INVALID_PREVIEW_IMAGE = "assets/invalid_preview.png"

    WHITE_STONE_IMAGE = "assets/white_stone.png"

    BLACK_STONE_IMAGE = "assets/black_stone.png"

    """ Constants. """

    GOBAN_SIZE = 9

    PIECE_SIZE = 42

    PADDING = 5

    ''' Finds the real position of a stone on the goban. '''

    @classmethod
    def get_real_position(cls, position):
        # Position on the X axis.
        x = (position[0] * MainUserInterface.PIECE_SIZE) + MainUserInterface.PADDING

        # Position on the Y axis.
        y = (position[1] * MainUserInterface.PIECE_SIZE) + MainUserInterface.PADDING

        return x, y

    def __init__(self):
        super().__init__("Go", MainUserInterface.WINDOW_SIZE)

        self.goban = Goban(MainUserInterface.GOBAN_SIZE)

        self.player = Color.BLACK

        # Position of the cursor.
        self.cursor = (0, 0)

        # Cursor modes.
        self.cursor_place_occupied = False

        self.cursor_invalid = False

        # Spots available for the cursor.
        self.spots = []

        for x in range(MainUserInterface.GOBAN_SIZE):
            for y in range(MainUserInterface.GOBAN_SIZE):

                position = MainUserInterface.get_real_position((x, y))

                # Create the spot.
                self.spots.append(
                    pygame.Rect(position[0], position[1], MainUserInterface.PIECE_SIZE, MainUserInterface.PIECE_SIZE))

    def event(self, event):
        for spot in self.spots:
            if spot.collidepoint(pygame.mouse.get_pos()):
                x = int((spot.left - MainUserInterface.PADDING) / MainUserInterface.PIECE_SIZE)
                y = int((spot.top - MainUserInterface.PADDING) / MainUserInterface.PIECE_SIZE)

                new_position = (x, y)

                if self.cursor != new_position:
                    self.cursor = new_position

                    self.cursor_place_occupied = self.goban.has_stone(self.cursor)
                    self.cursor_invalid = False

                    if not self.cursor_place_occupied:
                        self.cursor_invalid = not self.goban.valid_move(self.cursor, self.player)

        if event.type == pygame.MOUSEBUTTONUP:
            if event.button == 1:
                self.__put_stone()

    def update(self):
        pass

    def render(self):
        # Render the background.
        self.__render_image(MainUserInterface.BACKGROUND_IMAGE, (0, 0), MainUserInterface.WINDOW_SIZE)

        representation = self.goban.get_representation()

        for x in range(MainUserInterface.GOBAN_SIZE):
            for y in range(MainUserInterface.GOBAN_SIZE):
                stone = representation[y][x]

                if stone is not None:
                    if stone.color == Color.WHITE:
                        asset = MainUserInterface.WHITE_STONE_IMAGE
                    else:
                        asset = MainUserInterface.BLACK_STONE_IMAGE

                    position = MainUserInterface.get_real_position(stone.position)

                    self.__render_image(asset, position, (MainUserInterface.PIECE_SIZE, MainUserInterface.PIECE_SIZE))

        # Preview rendering.
        if not self.cursor_place_occupied:
            if self.cursor_invalid:
                preview = MainUserInterface.INVALID_PREVIEW_IMAGE
            else:
                if self.player == Color.WHITE:
                    preview = MainUserInterface.WHITE_PREVIEW_IMAGE
                else:
                    preview = MainUserInterface.BLACK_PREVIEW_IMAGE

            self.__render_image(preview, MainUserInterface.get_real_position(self.cursor),
                               (MainUserInterface.PIECE_SIZE, MainUserInterface.PIECE_SIZE))

    ''' Renders an image. '''

    def __render_image(self, image, position, size):
        background = pygame.image.load(image)

        self.screen.blit(background, pygame.Rect(position, size))

    ''' Put a stone on the goban. '''

    def __put_stone(self):

        if self.goban.valid_move(self.cursor, self.player):

            self.goban.put_stone(Stone(self.cursor, self.player))

            # Switch player.
            self.player = Color.find_opposite(self.player)

            # Set place as occupied (UI)
            self.cursor_place_occupied = True

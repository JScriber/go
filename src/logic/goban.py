from typing import Tuple, List, Optional

import numpy as np

from src.logic.color import Color
from src.logic.group import Group
from src.logic.stone import Stone
from src.logic.utils.position import Position


class Goban:

    def __init__(self, size):
        self.__size = size
        self.__representation = np.empty((size, size), dtype=Stone)

    def __str__(self):
        string_representation = ""

        for x in range(self.__size - 1):
            for y in range(self.__size - 1):
                stone = self.__get_stone((y, x))

                if stone is None:
                    string_representation += " "
                else:
                    string_representation += str(stone)

                string_representation += "|"

            string_representation += "\n"

        return string_representation

    ''' Goban representation. '''

    def get_representation(self):
        return self.__representation

    ''' Put a stone. '''

    def put_stone(self, stone: Stone) -> int:
        position = stone.position
        score = 0

        if self.valid_move(position, stone.color):

            # Put the stone.
            self.__representation[position[0]][position[1]] = stone

            # Found surrounding groups.
            groups = self.__find_surrounding_groups(stone)

            # Create the group for the stone.
            groups.append(Group(stone))

            # Merge the groups all together.
            Group.mergeGroups(groups, attach_stones=True)

            # Check dead opposite groups.
            score = self.__check_groups_freedoms(Color.find_opposite(stone.color))

        return score

    ''' Says if the stone is a valid move. '''

    def valid_move(self, position: Tuple[int, int], color: Color) -> bool:
        return not self.has_stone(position) and self.__will_create_viable_group(position, color)

    ''' Says if the position is occupied. '''

    def has_stone(self, position: Tuple[int, int]) -> bool:
        return self.__get_stone(position) is not None

    ''' Target a stone on the goban. '''

    def __get_stone(self, position: Tuple[int, int]) -> Optional[Stone]:
        return self.__representation[position[0]][position[1]]

    ''' Finds the groups that surrounds a stone. '''

    def __find_surrounding_groups(self, stone: Stone) -> List[Group]:
        groups = []

        for neighbour in self.__find_stone_neighbours(stone):
            if neighbour is not None and neighbour.color == stone.color and neighbour.group not in groups:
                groups.append(neighbour.group)

        return groups

    ''' Check the health of all the groups of the given color. '''

    def __check_groups_freedoms(self, color: Color) -> int:
        score = 0

        for group in self.__find_groups_without_freedom(color):
            stones = group.stones
            score = len(stones)

            for stone in stones:
                position = stone.position

                self.__representation[position[0]][position[1]] = None
                del stone

            del group

        return score

    ''' Finds all the groups for a given color that must be deleted. '''

    def __find_groups_without_freedom(self, color: Color) -> List[Group]:
        groups = []

        for group in self.__find_groups(color):
            if not self.__is_alive(group):
                groups.append(group)

        return groups

    ''' Finds all the groups for a given color. '''

    def __find_groups(self, color: Color) -> List[Group]:
        groups = []

        for x in range(self.__size):
            for y in range(self.__size):
                stone = self.__get_stone((x, y))

                if stone is not None and stone.color == color and stone.group not in groups:
                    groups.append(stone.group)

        return groups

    ''' Says if a group has at least one freedom. '''

    def __is_alive(self, group: Group) -> bool:

        for stone in group.stones:
            if None in self.__find_stone_neighbours(stone):
                return True

        return False

    ''' Lists the neighbours of a stone. '''

    def __find_stone_neighbours(self, stone: Stone) -> List[Stone]:
        neighbours = []
        interval = (0, self.__size - 1)

        def check_position(vec_x, vec_y):
            altered_position = Position.apply_vector(stone.position, (vec_x, vec_y), interval)

            if altered_position is not None:
                neighbours.append(self.__get_stone(altered_position))

        # Left.
        check_position(-1, 0)

        # Top.
        check_position(0, -1)

        # Right.
        check_position(1, 0)

        # Bottom.
        check_position(0, 1)

        return neighbours

    ''' Says if the stone will result into a viable group. '''

    def __will_create_viable_group(self, position: Tuple[int, int], color: Color) -> bool:
        stone = Stone(position, color)
        groups = self.__find_surrounding_groups(stone)

        # Create the group for the stone.
        groups.append(Group(stone))

        # Merge the groups all together.
        group_cluster = Group.mergeGroups(groups, attach_stones=False)

        # Temporary put the stone.
        self.__representation[position[0]][position[1]] = stone

        is_alive = self.__is_alive(group_cluster)
        damaged_foos = self.__find_groups_without_freedom(Color.find_opposite(color))

        self.__representation[position[0]][position[1]] = None

        return is_alive or len(damaged_foos) > 0

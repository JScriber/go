import pygame
import sys
from abc import ABC, abstractmethod


class UserInterface(ABC):

    def __init__(self, title, window_size):
        pygame.init()
        pygame.display.set_caption(title)

        self.screen = pygame.display.set_mode(window_size)

    ''' Launch the game. '''

    def run(self, fps=10):
        clock = pygame.time.Clock()

        while True:
            self._event()
            self.update()
            self._render()

            clock.tick(fps)

    ''' Exit the game. '''

    def exit(self):
        sys.exit()

    ''' Execute the render then updates the view. '''

    def _render(self):
        self.render()
        pygame.display.update()

    ''' Execute the event resolution for each event. '''

    def _event(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.exit()

            self.event(event)

    ''' Event resolution. '''

    @abstractmethod
    def event(self, event):
        pass

    ''' Action to execute after each cycle. '''

    @abstractmethod
    def update(self):
        pass

    ''' Drawing actions. '''

    @abstractmethod
    def render(self):
        pass

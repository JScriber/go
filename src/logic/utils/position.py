
class Position:

    @classmethod
    def apply_vector(cls, position, vector, interval):
        x = position[0] + vector[0]
        y = position[1] + vector[1]

        # Return none if not in the interval.
        if x < interval[0] or x > interval[1] or y < interval[0] or y > interval[1]:
            return None

        return x, y

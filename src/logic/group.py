from typing import List

from src.logic.stone import Stone


class Group:

    """ Merges all the groups into one. """

    @classmethod
    def mergeGroups(cls, groups: List["Group"], attach_stones: bool) -> "Group":
        cluster = Group()

        for group in groups:
            for stone in group.stones:
                cluster.stones.append(stone)

                if attach_stones:
                    stone.group = cluster

        return cluster

    def __init__(self, initial_stone: Stone = None):
        self.stones = []

        if initial_stone is not None:
            self.__attach_stone(initial_stone)

    ''' Add a stone to the group. '''

    def __attach_stone(self, stone: Stone):
        stone.group = self
        self.stones.append(stone)

from enum import Enum


class Color(Enum):
    WHITE = 1
    BLACK = 2

    @classmethod
    def find_opposite(cls, color):
        if color == Color.WHITE:
            return Color.BLACK
        else:
            return Color.WHITE


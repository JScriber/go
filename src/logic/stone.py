from typing import Tuple

from src.logic.color import Color

""" A stone is defined by its color, position and group. """


class Stone:

    def __init__(self, position: Tuple[int, int], color: Color):
        self.position = position
        self.color = color
        self.group = None

    def __str__(self):
        representation = ""

        if self.color == Color.WHITE:
            representation = "○"
        elif self.color == Color.BLACK:
            representation = "●"

        return representation
